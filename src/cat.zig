const std = @import("std");

pub const Cat = struct {
    const Self = @This();
    const log = std.log.scoped(.cat);
};
