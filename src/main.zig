const std = @import("std");

const options = @import("options.zig");
const s = @import("c.zig").sdl;

pub var window: *s.SDL_Window = undefined;
pub var renderer: *s.SDL_Renderer = undefined;

pub fn main() !void {
    const ops = options.options;
    const log = std.log.scoped(.main);

    if (s.SDL_Init(s.SDL_INIT_EVERYTHING) != 0) {
        log.err("Failed to initialize SDL: {s}", .{s.SDL_GetError()});
        return error.Init;
    }
    defer s.SDL_Quit();

    window = blk: {
        const win = s.SDL_CreateWindow(
            ops.window.title,
            ops.window.pos_x,
            ops.window.pos_y,
            ops.window.width,
            ops.window.height,
            ops.window.flags,
        );

        if (win == null) {
            log.err("Failed to create window: {s}", .{s.SDL_GetError()});
            return error.CreateWindow;
        }

        break :blk win.?;
    };
    defer s.SDL_DestroyWindow(window);

    renderer = whos: {
        const ren = s.SDL_CreateRenderer(window, -1, ops.renderer.flags);
        if (ren == null) {
            log.err("Failed to create renderer: {s}", .{s.SDL_GetError()});
            return error.CreateRenderer;
        }

        break :whos ren.?; // UEEEEeeeEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    };
    defer s.SDL_DestroyRenderer(renderer);

    var game = try @import("game.zig").Game.init();
    game.run();
}
