const s = @import("c.zig").sdl;

pub const options = struct {
    time: struct {
        target_fps: comptime_int = 60,
    } = .{},

    window: struct {
        title: [*c]const u8 = "Cat Petting Game",
        pos_x: u32 = s.SDL_WINDOWPOS_UNDEFINED,
        pos_y: u32 = s.SDL_WINDOWPOS_UNDEFINED,
        width: u32 = 300,
        height: u32 = 400,
        flags: u32 = s.SDL_WINDOW_SHOWN,
    } = .{},

    renderer: struct {
        background_color: s.SDL_Color = .{ .r = 0, .g = 0, .b = 0, .a = 0 },
        flags: u32 = s.SDL_RENDERER_ACCELERATED,
    } = .{},

    keyboard: struct {
        emergency_quit: s.SDL_Keycode = s.SDLK_ESCAPE,
    } = .{},

    mouse: struct {
        select: u1 = s.SDL_BUTTON_LEFT,
    } = .{},

    fn init() @This() {
        return @This(){};
    }
}.init();
