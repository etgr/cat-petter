const std = @import("std");

const s = @import("c.zig").sdl;

var state: struct {
    buttons: u32,
    x: c_int,
    y: c_int,
} = undefined;

pub fn updateState() void {
    state.buttons = s.SDL_GetMouseState(&state.x, &state.y);
}

pub fn isButtonDown(comptime action_name: []const u8) bool {
    const ops = @import("options.zig").options;

    const sdl_button: u5 = @field(ops.mouse, action_name);

    // Not working
    // const real_button = s.SDL_BUTTON(sdl_button);
    // const real_button = @intCast(u32, s.SDL_BUTTON(sdl_button));
    const real_button: u32 = @as(u32, 1) << (sdl_button - 1);

    return state.buttons & real_button > 0;
}

pub fn pos() struct { x: @TypeOf(state.x), y: @TypeOf(state.y) } {
    return .{
        .x = state.x,
        .y = state.y,
    };
}
