const std = @import("std");

const s = @import("c.zig").sdl;
const kbd = @import("keyboard.zig");
const mouse = @import("mouse.zig");

const State = enum(u8) {
    quitting,
    title_screen,
    running,
    // transition: struct { from: usize, to: usize, ns_remaining: u8 },

    _,
};

pub const Game = struct {
    const Self = @This();
    const log = std.log.scoped(.game);

    timer: std.time.Timer,
    rng: std.rand.DefaultPrng,

    state: State,

    total_pets: u128,

    pub fn init() !Self {
        var timer = std.time.Timer.start() catch |err| {
            log.err("Failed to start the timer: {}", .{err});
            return error.StartTimer;
        };

        return Self{
            .timer = timer,
            .rng = std.rand.DefaultPrng.init(0),

            .state = .title_screen,

            .total_pets = 0,
        };
    }

    pub fn run(self: *Self) void {
        // 1s / fps, but in nanoseconds because of std.time.sleep.
        const target_frame_ns: comptime_int = 1 * std.time.ns_per_s /
            @import("options.zig").options.time.target_fps;

        while (self.state != .quitting) {
            self.timer.reset();

            self.handleEvents();
            self.update(self.timer.read());
            self.draw();

            const ns = self.timer.read();
            if (ns < target_frame_ns) {
                std.time.sleep(target_frame_ns - ns);
            }
        }
    }

    fn handleEvents(self: *Self) void {
        var event: s.SDL_Event = undefined;

        while (s.SDL_PollEvent(&event) != 0) {
            switch (event.@"type") {
                s.SDL_QUIT => self.state = .quitting,

                else => {},
            }
        }
    }

    fn update(self: *Self, elapsed_ns: u64) void {
        kbd.updateState();
        mouse.updateState();

        switch (self.state) {
            .quitting => {},
            .title_screen => self.updateTitleScreen(),
            .running => self.updateRunning(),

            else => {
                log.emerg("Cannot update state {}", .{@tagName(self.state)});
                @panic("Unable to handle state");
            },
        }

        // Because it's not an emergency quit if it's only handled in some
        // states.
        if (kbd.isKeyDown("emergency_quit")) {
            self.state = .quitting;
        }
    }

    fn updateTitleScreen(self: *Self) void {
        if (mouse.isButtonDown("select")) {
            self.state = .running;
        }
    }

    fn updateRunning(self: *Self) void {
        if (mouse.isButtonDown("select")) {
            self.total_pets += 1;
            log.info("pets {}", .{self.total_pets});
        }
    }

    fn draw(self: *Self) void {
        switch (self.state) {
            .quitting => {},
            .title_screen => self.drawTitleScreen(),
            .running => self.drawRunning(),

            else => {
                log.emerg("Cannot draw state {}", .{@tagName(self.state)});
                @panic("Unable to handle state");
            },
        }
    }

    fn drawTitleScreen(self: *Self) void {
        const r = @import("root").renderer;
        const o = @import("options.zig").options;

        _ = s.SDL_SetRenderDrawColor(r, 255, 0, 0, 0);
        _ = s.SDL_RenderClear(r);
        s.SDL_RenderPresent(r);
    }

    fn drawRunning(self: *Self) void {
        const r = @import("root").renderer;
        const o = @import("options.zig").options;

        if (s.SDL_SetRenderDrawColor(
            r,
            o.renderer.background_color.r,
            o.renderer.background_color.g,
            o.renderer.background_color.b,
            o.renderer.background_color.a,
        ) != 0) log.err(
            "Failed to set renderer draw color: {}",
            .{s.SDL_GetError()},
        );

        if (s.SDL_RenderClear(r) != 0) log.err(
            "Failed to clear renderer: {}",
            .{s.SDL_GetError()},
        );

        s.SDL_RenderPresent(r);
    }
};
