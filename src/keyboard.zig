const std = @import("std");

const s = @import("c.zig").sdl;

var state: []const u8 = undefined;

pub fn updateState() void {
    var num_keys: c_int = -1;
    var kbd = s.SDL_GetKeyboardState(&num_keys);

    std.debug.assert(num_keys > 0);

    state = kbd[0..@intCast(usize, num_keys)];
}

pub fn isKeyDown(comptime action_name: []const u8) bool {
    // Ignore emergency_quit in non-debug builds.
    if (@import("builtin").mode != .Debug and
        std.mem.eql(u8, action_name, "emergency_quit"))
    {
        return false;
    }

    const ops = @import("options.zig").options;

    const key = @field(ops.keyboard, action_name);
    const scancode = s.SDL_GetScancodeFromKey(key);
    const idx = @enumToInt(scancode);

    std.debug.assert(idx > 0 and idx < state.len);

    return state[@intCast(usize, idx)] > 0;
}
